package com.example;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.InputStream;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    private void run() {
        InputStream schemaIn = this.getClass().getResourceAsStream("/examples/example-1/schema.json");
        InputStream dataIn = this.getClass().getResourceAsStream("/examples/example-1/data.json");

        Schema schema = SchemaLoader.load(new JSONObject(new JSONTokener(schemaIn)));

        try {
            schema.validate(new JSONObject(new JSONTokener(dataIn)));
        } catch (ValidationException e) {
            e.getCausingExceptions().stream()
                .map(ValidationException::getMessage)
                .forEach(System.out::println);
        }
    }
}
